FROM openjdk:15
VOLUME tmp
ADD target/myTunes-0.0.1-SNAPSHOT.jar mytunes.jar
ENTRYPOINT ["java", "-jar", "/mytunes.jar"]