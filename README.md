# Assignment Four

An iTunes rip-off

### API

This project contains some API endpoints:

* GET /api/customers - gets all customers
* POST /api/customers - add a new customer
* PUT /api/customers/{id] - update a current customer
* GET /api/customers/total?sort=country:desc - gets every country with their numbers off customers in descending order
* GET /api/customers/highest/spenders - gets customers who are the highest spenders in descending order
* GET /api/customers/{id}/popular/genre - gets the most popular genres for a specific customer

### Views

This project contains two views:

* The home screen shows 5 random artists, songs and genres. It also contains a searchbar which guides you to a search result view
* The search result view lists up all the tracks, with their album, artist and genre, that match the search

### How to run
You can simply go to https://mytunes-official.herokuapp.com/ to view the site. Or, if you would like to run it locally, you could clone the project and run it using the tools
of an IDE.
