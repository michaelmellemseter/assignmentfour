package com.example.myTunes.models;

public class Artist {
    private String artistId;
    private String name;

    public Artist(String artistId, String name) {
        this.artistId = artistId;
        this.name = name;
    }

    public String getArtistId() {
        return artistId;
    }

    public String getName() {
        return name;
    }
}
