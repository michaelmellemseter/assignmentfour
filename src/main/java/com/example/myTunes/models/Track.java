package com.example.myTunes.models;

public class Track {
    private String trackId;
    private String name;

    public Track(String trackId, String name) {
        this.trackId = trackId;
        this.name = name;
    }

    public String getTrackId() {
        return trackId;
    }

    public String getName() {
        return name;
    }
}
