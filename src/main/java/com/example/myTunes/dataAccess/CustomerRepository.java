package com.example.myTunes.dataAccess;

import com.example.myTunes.models.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class CustomerRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("" +
                            "SELECT CustomerId, FirstName,LastName, Country, PostalCode, Phone, Email FROM Customer");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting customers.");
            System.out.println(ex.toString());
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return customers;
    }

    public boolean addNewCustomer(Customer customer) {
        boolean wentWell = false;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer (CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());
            // Execute Statement
            int result = preparedStatement.executeUpdate();
            wentWell = result != 0;
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
            wentWell = false;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
                wentWell = false;
            }
        }
        return wentWell;
    }

    public boolean updateCustomer(Customer customer) {
        boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("" +
                    "UPDATE Customer SET CustomerId=?, FirstName=?, LastName=?, Country=?, PostalCode=?, Phone=?, Email=? WHERE CustomerId=?");
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.setString(8, customer.getCustomerId());

            int result = preparedStatement.executeUpdate();
            success = result != 0;
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when updating customer");
            System.out.println(ex.toString());
        }
        finally {
            try{
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return success;
    }

    public LinkedHashMap<String, Integer> getAmountPrCountry() {
        // Hashmap for country and number of customers
        LinkedHashMap<String, Integer> amountCountry = new LinkedHashMap<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT country, COUNT(*) FROM Customer GROUP BY country ORDER BY COUNT(*) DESC");
            ResultSet resultSet = preparedStatement.executeQuery();

            // Add result to the hashmap
            while(resultSet.next()) {
                amountCountry.put(resultSet.getString("country"), resultSet.getInt("COUNT(*)"));
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting customers.");
            System.out.println(ex.toString());
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return amountCountry;
    }

    public LinkedHashMap<String, String> getHighestSpenders() {
        LinkedHashMap<String, String> highestSpenders = new LinkedHashMap<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("" +
                            "SELECT Customer.CustomerId AS customerId, Customer.FirstName AS firstName, Customer.LastName AS lastName , SUM(Invoice.Total) AS totalSum " +
                            "FROM Customer " +
                            "INNER JOIN Invoice " +
                            "ON Customer.CustomerId = Invoice.CustomerId " +
                            "GROUP BY Customer.CustomerId " +
                            "ORDER BY SUM(Invoice.Total) DESC ");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                highestSpenders.put(
                        resultSet.getString(
                                "customerId") + ": " + resultSet.getString("firstName") + " " +resultSet.getString("lastName"), resultSet.getString("totalSum"
                        )
                );
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting the highest spenders.");
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return highestSpenders;
    }

    public ArrayList<String> getMostPopularGenre(String id) {
        // Arraylist for the most popular genres for this customer
        ArrayList<String> genres = new ArrayList<>();
        int mostPopular = 0;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("" +
                            "SELECT genre.name AS name, COUNT(genre.genreid) FROM ((((customer \n" +
                            "INNER JOIN invoice ON customer.customerid = invoice.customerid)\n" +
                            "INNER JOIN invoiceLine ON invoice.invoiceid = invoiceLine.invoiceid)\n" +
                            "INNER JOIN track ON invoiceLine.trackid = track.trackid)\n" +
                            "INNER JOIN genre ON track.genreid = genre.genreid)\n" +
                            "WHERE customer.customerid = ?\n" +
                            "GROUP BY genre.genreid\n" +
                            "ORDER BY COUNT(genre.genreid) DESC");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            boolean first = true;
            while(resultSet.next()) {
                if (first) {  // Adding the most popular genre to the arraylist
                    genres.add(resultSet.getString("name"));
                    mostPopular = resultSet.getInt("COUNT(genre.genreid)");
                    first = false;
                } else {  // Adding more genres if there is a tie
                    if (mostPopular == resultSet.getInt("COUNT(genre.genreid)")) {
                        genres.add(resultSet.getString("name"));
                    } else {
                        break;
                    }
                }
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting customers.");
            System.out.println(ex.toString());
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return genres;
    }
}
