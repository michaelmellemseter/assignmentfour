package com.example.myTunes.dataAccess;

import com.example.myTunes.models.Genre;

import java.sql.*;
import java.util.ArrayList;

public class GenreRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public ArrayList<Genre> getRandomGenres(int number) {
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("" +
                            "SELECT * FROM Genre ORDER BY RANDOM() LIMIT ?");
            preparedStatement.setInt(1, number);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                genres.add(
                        new Genre(
                                resultSet.getString("GenreId"),
                                resultSet.getString("Name")
                        ));
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting " + number + " random genres");
            System.out.println(ex.toString());
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return genres;
    }
}
