package com.example.myTunes.dataAccess;

import com.example.myTunes.models.Track;

import java.sql.*;
import java.util.ArrayList;

public class TrackRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public ArrayList<Track> getRandomTracks(int number) {
        ArrayList<Track> tracks = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("" +
                            "SELECT TrackId, Name FROM Track ORDER BY RANDOM() LIMIT ?");
            preparedStatement.setInt(1, number);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                tracks.add(
                        new Track(
                                resultSet.getString("TrackId"),
                                resultSet.getString("Name")
                        ));
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting " + number + " random tracks");
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return tracks;
    }

    public ArrayList<ArrayList<String>> getSearchResult(String search) {
        // Arraylist with an arraylist for information of songs that matches the search
        ArrayList<ArrayList<String>> tracks = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("" +
                            "SELECT track.name as track, album.title as album, artist.name as artist, genre.name as genre FROM (((track \n" +
                            "INNER JOIN genre ON track.genreid = genre.genreid)\n" +
                            "INNER JOIN album ON track.albumid = album.albumid)\n" +
                            "INNER JOIN artist ON album.artistid = artist.artistid) ");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                if (resultSet.getString("track").toLowerCase().contains(search)) {  //Checking if the track matches the search
                    ArrayList<String> trackInfo = new ArrayList<>();
                    trackInfo.add(resultSet.getString("track"));
                    trackInfo.add(resultSet.getString("album"));
                    trackInfo.add(resultSet.getString("artist"));
                    trackInfo.add(resultSet.getString("genre"));
                    tracks.add(trackInfo);
                }
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting customers.");
            System.out.println(ex.toString());
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return tracks;
    }
}
