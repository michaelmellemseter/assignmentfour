package com.example.myTunes.dataAccess;

import com.example.myTunes.models.Artist;

import java.sql.*;
import java.util.ArrayList;

public class ArtistRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public ArrayList<Artist> getRandomArtists(int number) {
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement =
                    conn.prepareStatement("" +
                            "SELECT * FROM Artist ORDER BY RANDOM() LIMIT ?");
            preparedStatement.setInt(1, number);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                artists.add(
                        new Artist(
                                resultSet.getString("ArtistId"),
                                resultSet.getString("Name")
                        ));
            }
        }
        catch(SQLException ex) {
            System.out.println("Something went wrong when getting " + number + " random artists");
            System.out.println(ex.toString());
        }
        finally{
            try {
                conn.close();
            }
            catch(SQLException ex) {
                System.out.println("Something went wrong when closing the connection");
                System.out.println(ex.toString());
            }
        }
        return artists;
    }
}
