package com.example.myTunes.controllers;

import com.example.myTunes.dataAccess.CustomerRepository;
import com.example.myTunes.models.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;

@RestController
@RequestMapping(value = "api/customers")
public class CustomerController {
    private CustomerRepository customerRepository = new CustomerRepository();

    // Get all customers
    @GetMapping
    public ResponseEntity<ArrayList<Customer>> getAllCustomers() {
        ArrayList<Customer> customers = customerRepository.getAllCustomers();
        if(customers.isEmpty()) // If there are no customers in the list, then something went wrong when getting them
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        return ResponseEntity.ok(customers); // Ok, return customers
    }

    // Add a new customer
    @PostMapping
    public ResponseEntity<Boolean> addNewCustomer(@RequestBody Customer customer) {
        Boolean success = customerRepository.addNewCustomer(customer);
        if(!success) return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        return ResponseEntity.ok(true);
    }

    // Update a customer
    @PutMapping("/{id}")
    public ResponseEntity<Boolean> updateCustomer(@PathVariable String id, @RequestBody Customer customer) {
        Boolean success = customerRepository.updateCustomer(customer);
        if(!success) return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        return ResponseEntity.ok(true);
    }

    /* Get the number of customers pr country in descending order
    The endpoint is called GET api/customers/total, because we could add just returning the total
    count of customer. Now, it will return total by country by default.
    We could also add more options for what to pass in with the query string.
    We could for example want to, in addition to sorting by country, sort by postal code,
    or something else.
    Using GET api/customer/total?sort=<what to sort by>:<order> */
    @GetMapping("/total")
    public ResponseEntity<LinkedHashMap<String, Integer>> getAmountPrCountry(@RequestParam(value = "sort", defaultValue = "country") String sort) {
        if(sort.equals("country:desc")) { // If the request params are ok, try to get the data
            LinkedHashMap<String, Integer> data = customerRepository.getAmountPrCountry();
            if(data.isEmpty()) // Return 500 if the data is empty
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.ok(data); // Return data with code 200
        }
        else // We would have to handle different cases, but now we just return 400 Bad Request (in some cases we could've returned 501 instead)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /* Get the highest spenders
    Set up like /highest/spenders, as we possibly could also get something else "highest" other than spenders (total millieconds,
    total bytes, etc...) */
    @GetMapping("/highest/spenders")
    public ResponseEntity<LinkedHashMap<String, String>> getHighestSpenders() {
        LinkedHashMap<String, String> data = customerRepository.getHighestSpenders();
        if(data.isEmpty()) return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        return ResponseEntity.ok(data);
    }

    // Get the most popular genre for a specific customer
    @GetMapping("/{id}/popular/genre")
    public ResponseEntity<ArrayList<String>> getMostPopularGenre(@PathVariable String id) {
        ArrayList<String> data = customerRepository.getMostPopularGenre(id);
        if(data.isEmpty()) return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        return ResponseEntity.ok(data);
    }
}

