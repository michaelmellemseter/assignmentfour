package com.example.myTunes.controllers;

import com.example.myTunes.dataAccess.ArtistRepository;
import com.example.myTunes.dataAccess.GenreRepository;
import com.example.myTunes.dataAccess.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    final int NUMBER_OF_RANDOMS = 5;
    ArtistRepository arep = new ArtistRepository();
    TrackRepository trep = new TrackRepository();
    GenreRepository grep = new GenreRepository();

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("artists", arep.getRandomArtists(NUMBER_OF_RANDOMS));
        model.addAttribute("tracks", trep.getRandomTracks(NUMBER_OF_RANDOMS));
        model.addAttribute("genres", grep.getRandomGenres(NUMBER_OF_RANDOMS));
        return "index";
    }
}
