package com.example.myTunes.controllers;

import com.example.myTunes.dataAccess.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchResultController {
    private TrackRepository trackRepository = new TrackRepository();

    // Get the search result site with the results from the search
    @GetMapping("/search")
    public String searchResult(@RequestParam(value = "term", defaultValue = "") String term, Model model) {
        String trimmedTerm = term.trim(); // Remove whitespaces so that we get results even with whitespaces in front/end
        model.addAttribute("trackInfos", trackRepository.getSearchResult(trimmedTerm));
        model.addAttribute("term", trimmedTerm);
        return "searchResult";
    }
}
